const {
  createRevolvingAccount,
  takeLoan,
  findOverdueLoans,
  repayLoan
} = require('../lib/index');
const { addDays } = require('date-fns');

function createAccountWithDefaultTestOptions() {
  const acc = createRevolvingAccount({
    principalCredit: 10000,
    repaymentWindow: 7,
    interestSettings: {
      interestCalculationMethod: 'FLAT',
      interestTimePeriodCountCriteria: 'REPAYMENT_PERIODICITY',
      interestType: 'COMPOUND',
      interestRateSettings: {
        daysInYear: 365,
        interestChargeFrequency: 'DAILY',
        interestRate: 0.14
      }
    }
  });
  return acc;
}

describe('Revolving loan', () => {
  it('creates account with default parameters ', () => {
    const acc = createRevolvingAccount({});
    expect(acc).toHaveProperty('principalCredit', expect.any(Number));
    expect(acc).toHaveProperty('remainingCredit', expect.any(Number));
    expect(acc).toHaveProperty('outstandingBalance', expect.any(Number));
    expect(acc).toHaveProperty('interestSettings', expect.any(Object));
    expect(acc).toHaveProperty('repaymentWindow', expect.any(Number));
    expect(acc).toHaveProperty('accountCreatedAt', expect.any(Date));
    expect(acc).toHaveProperty('penaltySettings', expect.any(Object));
    expect(acc).toHaveProperty('loans', []);
    expect(acc).toHaveProperty('repayments', []);
  });

  it('creates account with some of overridden parameters ', () => {
    const acc = createRevolvingAccount({
      principalCredit: 10000,
      repaymentWindow: 30
    });
    expect(acc).toHaveProperty('principalCredit', 10000);
    expect(acc).toHaveProperty('remainingCredit', 10000);
    expect(acc).toHaveProperty('outstandingBalance', 0);
    expect(acc).toHaveProperty('interestSettings', expect.any(Object));
    expect(acc).toHaveProperty('repaymentWindow', 30);
    expect(acc).toHaveProperty('accountCreatedAt', expect.any(Date));
    expect(acc).toHaveProperty('penaltySettings', expect.any(Object));
    expect(acc).toHaveProperty('loans', []);
    expect(acc).toHaveProperty('repayments', []);
  });

  it('accounting is correct when withdrawing money from a revolving account for the first time', () => {
    let acc = createAccountWithDefaultTestOptions();
    acc = takeLoan(5000, acc);
    expect(acc.remainingCredit).toBe(5000);

    expect(acc.loans.length).toBe(1);
    expect(acc.loans[0].status).toBe('repayment-pending');

    expect(acc.loans[0].totalRepayable).toBeCloseTo(5012.58);
    expect(acc.loans[0].remainingRepayable).toBeCloseTo(5012.58);
    expect(acc.loans[0].amountRepaid).toBeCloseTo(0.0);
  });

  it('throws an error when trying to withdraw more than remaining credit', () => {
    const acc = createAccountWithDefaultTestOptions();

    expect(() => {
      acc.takeLoan(50000, acc);
    }).toThrow();
  });

  it('thorws an error if overdue loan exists', () => {
    let acc = createAccountWithDefaultTestOptions();
    expect(() => {
      acc = takeLoan(1000, acc, new Date('2020-01-01'));
    }).not.toThrow();

    expect(() => {
      takeLoan(
        2000,
        acc,
        addDays(new Date('2020-01-01'), acc.repaymentWindow + 1)
      );
    }).toThrow();
  });

  it("doesn't treat loans as overdue on the last day of repayment", () => {
    let acc = createAccountWithDefaultTestOptions();
    acc = takeLoan(1000, acc, '2020-01-01');
    const odLoans = findOverdueLoans(
      acc.loans,
      addDays(new Date('2020-01-01'), acc.repaymentWindow)
    );

    expect(odLoans.length).toBe(0);
  });

  it('repays full amount if enough repayment amount is given', () => {
    let acc = createAccountWithDefaultTestOptions();
    acc = takeLoan(1000, acc);
    acc = repayLoan(1002.52, acc);

    expect(acc.loans[0].status).toBe('repaid');
  });

  it('repays partially if not enough repayment amount is given', () => {
    let acc = createAccountWithDefaultTestOptions();
    acc = takeLoan(1000, acc);
    acc = repayLoan(300, acc);

    expect(acc.loans[0].status).toBe('partially-repaid');
    expect(acc.loans[0].remainingRepayable).toBeCloseTo(702.52, 1);
    expect(acc.loans[0].amountRepaid).toBeCloseTo(300);
  });

  it('can complete repayment for a loan with multiple repayment transactions', () => {
    let acc = createAccountWithDefaultTestOptions();
    acc = takeLoan(1000, acc);
    acc = repayLoan(300, acc);
    acc = repayLoan(700, acc);
    acc = repayLoan(2.52, acc);

    expect(acc.loans[0].status).toBe('repaid');
    expect(acc.loans[0].remainingRepayable).toBeCloseTo(0.0);
    expect(acc.loans[0].amountRepaid).toBeCloseTo(1002.52);
  });

  it('carries over a repayment amount towards other loans if exists', () => {
    let acc = createAccountWithDefaultTestOptions();
    acc = takeLoan(1000, acc);
    acc = takeLoan(500, acc);

    acc = repayLoan(1200, acc);

    expect(acc.loans.length).toBe(2);
    expect(acc.loans.filter(loan => loan.status === 'repaid').length).toBe(1);
    expect(
      acc.loans.filter(loan => loan.status === 'partially-repaid').length
    ).toBe(1);
    expect(acc.loans[0].totalRepayable).toBe(acc.loans[0].amountRepaid);
    expect(acc.loans[1].amountRepaid).toBeCloseTo(197.48);
  });
});
