import numeral from 'numeral';
import { INTEREST_CALCULATION_PERIOD, Loan } from './revolving-loan';
import {
  addDays,
  differenceInMonths,
  differenceInYears,
  differenceInWeeks
} from 'date-fns';

export function convertDaysTo(
  numberOfDays: number,
  convertTo: INTEREST_CALCULATION_PERIOD
): number {
  switch (convertTo) {
    case 'DAILY':
      return numberOfDays;
    case 'WEEKLY':
      return differenceInWeeks(new Date(), addDays(new Date(), numberOfDays));
    case 'MONTHLY':
      return differenceInMonths(new Date(), addDays(new Date(), numberOfDays));
    case 'YEARLY':
      return differenceInYears(new Date(), addDays(new Date(), numberOfDays));
  }
}

export function padNumbers(
  obj: Record<string, number>,
  attributes: string[]
): Record<string, number> {
  attributes.forEach(k => {
    obj[k] = Number(numeral(obj[k]).format('0.00'));
  });

  return obj;
}

function findRepaymentWindowExceededLoans(
  loans: Loan[],
  date: Date = new Date()
): Loan[] {
  return loans.filter(l => {
    const repaymentExceedDate = addDays(
      new Date(l.createdAt),
      l.repaymentWindow
    );
    return repaymentExceedDate < date;
  });
}

export function findUnpaidLoans(
  loans: Loan[],
  date: Date = new Date()
): Loan[] {
  const repaymentWindowExceededLoans = findRepaymentWindowExceededLoans(
    loans,
    date
  );
  return repaymentWindowExceededLoans.filter(
    loan => loan.status === 'repayment-pending'
  );
}
