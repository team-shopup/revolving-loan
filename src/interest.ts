import { InterestChargeFrequency, InterestType } from './revolving-loan';

interface InterestCalculationParams {
  principalAmount: number;
  interestRatePerYear: number;
  interestChargeFrequency: InterestChargeFrequency;
  time: number;
  daysInYear: number;
  interestType: InterestType;
}

export function calculateInterest({
  principalAmount,
  interestRatePerYear,
  interestChargeFrequency, // 'DAILY', 'WEEKLY', 'MONTHLY', 'YEARLY'
  time,
  daysInYear = 365,
  interestType
}: InterestCalculationParams): number {
  switch (interestChargeFrequency) {
    case 'DAILY':
      time = time / daysInYear;
      break;
    case 'WEEKLY':
      time = time / 52;
      break;
    case 'MONTHLY':
      time = time / 12;
      break;
    case 'YEARLY':
      break;
  }

  /*
   *  formula for simple interest = p(1 + it)
   * where p = principal
   * r = yearly interest rate
   * t = time
   * here `t` needs to be convereted to the correct format
   * if interest is calculated 'DAILY' | 'WEEKLY' | 'MONTHLY' etc.
   */
  if (interestType === 'SIMPLE') {
    return principalAmount * interestRatePerYear * time;
  }
  const NUMBER_OF_TIMES_INTEREST_IS_APPLIED_PER_INTERVAL = 1;

  /*
   * formula for compound interest
   * p(1 + r/n)^nt
   * p = principal
   * r = yearly interest rate
   * n = number of times interest gets compounded per interval
   * t = time elapsed (in year)
   * here `t` needs to be convereted to the correct format
   * if interest is calculated 'DAILY' | 'WEEKLY' | 'MONTHLY' etc.
   */
  return (
    principalAmount *
    Math.pow(
      interestRatePerYear / NUMBER_OF_TIMES_INTEREST_IS_APPLIED_PER_INTERVAL,
      NUMBER_OF_TIMES_INTEREST_IS_APPLIED_PER_INTERVAL * time
    )
  );
}

export function calculateInterestWithPrincipal({
  principalAmount,
  interestRatePerYear,
  interestChargeFrequency, // 'DAILY', 'WEEKLY', 'MONTHLY', 'YEARLY'
  time,
  daysInYear = 365,
  interestType
}: InterestCalculationParams): number {
  return (
    principalAmount +
    calculateInterest({
      principalAmount,
      interestRatePerYear,
      interestChargeFrequency,
      time,
      daysInYear,
      interestType
    })
  );
}
