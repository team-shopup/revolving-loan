import { RevolvingLoanAccount } from './../revolving-loan';
import { findUnpaidLoans } from './../utils';
import { all } from 'ramda';

interface ValidationMessage {
  isValid: boolean;
  errorMessage: string | null;
}

function validateLoanAmount(
  amount: number,
  revolvingAccount: RevolvingLoanAccount
): ValidationMessage {
  if (amount > revolvingAccount.remainingCredit) {
    return {
      isValid: false,
      errorMessage:
        'Cannot process the loan request because requested amount is greater than available account balance.'
    };
  }
  return {
    isValid: true,
    errorMessage: null
  };
}

function validateWithdrawlEligibility(
  revolvingAccount: RevolvingLoanAccount,
  withdrawlDate: Date
): ValidationMessage {
  const unpaidLoans = findUnpaidLoans(revolvingAccount.loans, withdrawlDate);
  if (unpaidLoans.length) {
    return {
      isValid: false,
      errorMessage:
        'Cannot process the loan request because not all loans have been repaid within the repayment window.'
    };
  }
  return {
    isValid: true,
    errorMessage: null
  };
}

export function validateLoanWithdrawl(
  loan: {
    amountRequested: number;
    withdrawlDate: Date;
  },
  acc: RevolvingLoanAccount
): boolean {
  return all(
    (validationMessage: ValidationMessage) => validationMessage.isValid === true
  )([
    validateLoanAmount(loan.amountRequested, acc),
    validateWithdrawlEligibility(acc, loan.withdrawlDate)
  ]);
}
