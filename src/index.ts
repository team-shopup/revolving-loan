import numeral from 'numeral';

import { calculateInterest, calculateInterestWithPrincipal } from './interest';
import { convertDaysTo, padNumbers, findUnpaidLoans } from './utils';
import {
  Loan,
  RevolvingLoanAccount,
  Repayment,
  LoanStatus,
  LoanEvent,
  LoanEventType,
  PenaltySettings,
  InterestSettings
} from './revolving-loan';

import { validateLoanWithdrawl } from './validations';
import { addDays, differenceInDays } from 'date-fns';

export const findOverdueLoans = findUnpaidLoans;

export function createRevolvingAccount({
  principalCredit = 50000,
  interestSettings = {
    interestCalculationMethod: 'FLAT',
    interestRateSettings: {
      daysInYear: 360,
      interestChargeFrequency: 'DAILY',
      interestRate: 5.0 / 100
    },
    interestTimePeriodCountCriteria: 'REPAYMENT_PERIODICITY',
    interestType: 'COMPOUND'
  },
  repaymentWindow = 15, // number of days before the debt has to be repaid,
  createdAt = new Date(),
  penaltySettings = {
    penaltyInterestRate: 0.14,
    penaltyFlatFee: 200,
    penaltyCalculationMethod: 'FLAT_FEE_WITH_INTEREST'
  },
  loans = [],
  repayments = []
}: {
  principalCredit?: number;
  interestSettings?: InterestSettings;
  repaymentWindow?: number;
  createdAt?: Date;
  penaltySettings?: PenaltySettings;
  loans?: Loan[];
  repayments?: Repayment[];
}): RevolvingLoanAccount {
  return {
    principalCredit,
    interestSettings,
    repaymentWindow,
    accountCreatedAt: createdAt,
    penaltySettings,
    loans,
    repayments,
    remainingCredit: principalCredit,
    outstanding: {
      principal: 0,
      interest: 0
    }
  };
}

function createLoan({
  amount,
  interestSettings,
  repaymentWindow,
  penaltySettings,
  date = new Date()
}: {
  amount: number;
  interestSettings: InterestSettings;
  repaymentWindow: number;
  penaltySettings: PenaltySettings;
  date: Date;
}): Loan {
  const {
    interestRateSettings: { interestRate, daysInYear, interestChargeFrequency },
    interestType
  } = interestSettings;

  const repayableInterest = calculateInterest({
    principalAmount: amount,
    interestRatePerYear: interestRate,
    interestChargeFrequency,
    time: convertDaysTo(repaymentWindow, interestChargeFrequency),
    interestType,
    daysInYear
  });
  return {
    amount,
    interestSettings,
    repaymentWindow,
    repaymentDeadline: addDays(date, repaymentWindow),
    penaltySettings,
    status: 'repayment-pending',
    createdAt: date,
    totalRepayable: amount + repayableInterest,
    remainingRepayable: amount + repayableInterest,
    repayable: {
      total: {
        principal: amount,
        interest: repayableInterest,
        total: amount + repayableInterest
      },
      outstanding: {
        principal: amount,
        interest: repayableInterest,
        total: amount + repayableInterest
      }
    },
    amountRepaid: 0.0,
    repaymentBreakdown: {
      interestRepaid: 0.0,
      principalRepaid: 0.0
    }
  };
}

export function takeLoan(
  amountRequested: number,
  revolvingAccount: RevolvingLoanAccount,
  date = new Date()
): RevolvingLoanAccount {
  if (
    !validateLoanWithdrawl(
      {
        amountRequested,
        withdrawlDate: date
      },
      revolvingAccount
    )
  ) {
    throw Error('Cannot proceed with the loan request as validation failed');
  }

  const {
    interestSettings,
    repaymentWindow,
    penaltySettings
  } = revolvingAccount;

  let loan = createLoan({
    amount: amountRequested,
    interestSettings,
    repaymentWindow,
    penaltySettings,
    date
  });

  /*
   TODO: hacks -- clean this up later
   */
  loan = (padNumbers((loan as unknown) as Record<string, number>, [
    'amount',
    'remainingRepayable',
    'totalRepayable',
    'amountRepaid'
  ]) as unknown) as Loan;

  return {
    ...revolvingAccount,
    remainingCredit: Number(
      numeral(revolvingAccount.remainingCredit - amountRequested).format('0.00')
    ),
    outstanding: calculateOutstanding([...revolvingAccount.loans, loan]),
    loans: [...revolvingAccount.loans, loan]
  };
}

// function shouldApplyPenalty(
//   { repaymentDeadline }: Loan,
//   repaymentDate: Date = new Date()
// ): boolean {
//   return repaymentDate > repaymentDeadline;
// }

// function findApplicablePenalty(
//   { penaltySettings, repaymentDeadline, repaymentWindow }: Loan,
//   repaymentDate: Date
// ) {
//   const daysPassedTillRepaymentPeriod = differenceInDays(
//     repaymentDate,
//     repaymentDeadline
//   );

//   calculateInterest({

//   })
// }

// function findTotalLoanRepayable({}: Loan, repaymentDate) {
//   if (shouldApplyPenalty(loan, repaymentDate)) {
//     const penalty = findApplicablePenalty(loan);
//   }
// }

const calculateRepaymentBreakdown = (
  repaidAmount: number,
  repayable: {
    total: number;
    principal: number;
    interest: number;
  }
): {
  principalRepaid: number;
  interestRepaid: number;
} => {
  const repaymentPercentage = repaidAmount / repayable.total;
  return {
    principalRepaid: repaymentPercentage * repayable.principal,
    interestRepaid: repaymentPercentage * repayable.interest
  };
};

function applyRepayment(
  loan: Loan,
  amount: number
): {
  loan: Loan;
  excess: number;
  creditsUnlocked: number;
  repaymentBreakdown: {
    principalRepaid: number;
    interestRepaid: number;
  };
} {
  const {
    status,
    repayable: {
      outstanding: { total: totalOutstanding },
      outstanding
    },
    repayable,
    repaymentBreakdown: { principalRepaid, interestRepaid }
  } = loan;

  if (status === 'repaid') {
    throw new Error('Cannot repay a loan that is already settled');
  }
  /*
   * TODO: Add penalty calculation here.
   */

  if (amount >= totalOutstanding) {
    return {
      loan: {
        ...loan,
        remainingRepayable: 0,
        status: 'repaid',
        amountRepaid: loan.amountRepaid + loan.remainingRepayable,
        repayable: {
          total: repayable.total,
          outstanding: {
            principal: 0.0,
            interest: 0.0,
            total: 0.0
          }
        },
        repaymentBreakdown: {
          principalRepaid: repayable.total.principal,
          interestRepaid: repayable.total.interest
        }
      },
      excess: amount - loan.remainingRepayable,
      creditsUnlocked:
        (loan.amount / loan.totalRepayable) * loan.remainingRepayable,
      repaymentBreakdown: calculateRepaymentBreakdown(
        totalOutstanding,
        repayable.total
      )
    };
  }

  if (amount < loan.remainingRepayable && amount > 0) {
    const {
      principalRepaid: principalRepaidWithThisRepayment,
      interestRepaid: interestRepaidWithThisRepayment
    } = calculateRepaymentBreakdown(amount, repayable.total);

    return {
      loan: {
        ...loan,
        remainingRepayable: loan.remainingRepayable - amount,
        status: 'partially-repaid',
        amountRepaid: loan.amountRepaid + amount,
        repayable: {
          total: repayable.total,
          outstanding: {
            principal: outstanding.principal - principalRepaidWithThisRepayment,
            interest: outstanding.interest - interestRepaidWithThisRepayment,
            total:
              outstanding.principal -
              principalRepaidWithThisRepayment +
              outstanding.interest -
              interestRepaidWithThisRepayment
          }
        },
        repaymentBreakdown: {
          principalRepaid: principalRepaid + principalRepaidWithThisRepayment,
          interestRepaid: interestRepaid + interestRepaidWithThisRepayment
        }
      },
      excess: 0.0,
      creditsUnlocked: (loan.amount / loan.totalRepayable) * amount,
      repaymentBreakdown: {
        principalRepaid: principalRepaidWithThisRepayment,
        interestRepaid: interestRepaidWithThisRepayment
      }
    };
  } else {
    return {
      loan: {
        ...loan
      },
      excess: 0.0,
      creditsUnlocked: 0.0,
      repaymentBreakdown: {
        principalRepaid: 0.0,
        interestRepaid: 0.0
      }
    };
  }
}

function breakdownRepayment(
  loan: Loan
): {
  principalRepaid: number;
  interestRepaid: number;
} {
  return {
    principalRepaid: (loan.amount / loan.totalRepayable) * loan.amountRepaid,
    interestRepaid:
      ((loan.totalRepayable - loan.amount) / loan.totalRepayable) *
      loan.amountRepaid
  };
}

function calculateOutstanding(
  loans: Loan[]
): {
  principal: number;
  interest: number;
} {
  return {
    principal: loans.reduce(
      (acc: number, { repaymentBreakdown: { principalRepaid }, amount }) =>
        acc + (amount - principalRepaid),
      0
    ),
    interest: loans.reduce(
      (
        acc,
        { repaymentBreakdown: { interestRepaid }, totalRepayable, amount }
      ) => acc + (totalRepayable - amount) - interestRepaid,
      0
    )
  };
}

export function repayLoan(
  repaymentAmount: number,
  account: RevolvingLoanAccount,
  repayDate = new Date()
): RevolvingLoanAccount {
  const repayment = {
    amount: repaymentAmount,
    createdAt: repayDate
  };

  const unpaidLoans = account.loans.filter(loan =>
    (['repayment-pending', 'partially-repaid'] as LoanStatus[]).includes(
      loan.status
    )
  );
  const result = unpaidLoans.reduce(
    (
      acc: {
        remainingRepaymentAmount: number;
        loans: Loan[];
        creditsUnlocked: number;
      },
      loan
    ) => {
      const { loan: repaidLoan, creditsUnlocked, excess } = applyRepayment(
        loan,
        acc.remainingRepaymentAmount
      );

      return {
        ...acc,
        loans: [...acc.loans, repaidLoan],
        creditsUnlocked: acc.creditsUnlocked + creditsUnlocked,
        remainingRepaymentAmount: excess
      };
    },
    {
      remainingRepaymentAmount: repaymentAmount,
      loans: [],

      creditsUnlocked: 0.0
    }
  );

  const loansAfterRepayments = [
    ...account.loans.filter(loan => loan.status === 'repaid'),
    ...result.loans
  ].map(loan => ({
    ...loan,
    repaymentBreakdown: breakdownRepayment(loan)
  }));

  return {
    ...account,
    loans: loansAfterRepayments,
    repayments: [...account.repayments, repayment],
    remainingCredit:
      result.creditsUnlocked + account.remainingCredit > account.principalCredit
        ? account.principalCredit
        : result.creditsUnlocked + account.remainingCredit,
    outstanding: calculateOutstanding(loansAfterRepayments)
  };
}

function aggregateLogs(acc: RevolvingLoanAccount): LoanEvent[] {
  const events: LoanEvent[] = [
    ...acc.loans.map(loan => ({
      ...loan,
      eventType: 'REQUEST_LOAN' as LoanEventType
    })),
    ...acc.repayments.map(repayment => ({
      ...repayment,
      eventType: 'REPAYMENT' as LoanEventType
    }))
  ];
  const eventLogs = events.sort(
    (a, b) => a.createdAt.getTime() - b.createdAt.getTime()
  ); // sorting the events in ascending order

  return eventLogs;
}

function applyActions(
  events: LoanEvent[],
  acc: RevolvingLoanAccount
): RevolvingLoanAccount {
  for (const [idx, event] of events.entries()) {
    try {
      switch (event.eventType) {
        case 'REQUEST_LOAN':
          acc = takeLoan(event.amount, acc);
          break;
        case 'REPAYMENT':
          acc = repayLoan(event.amount, acc);
          break;
      }
    } catch (err) {
      console.info(
        `Error occured while trying to apply actions.
        New account will have only the first ${idx.toString()} actions applied.`
      );
      break;
    }
  }

  return acc;
}

export function recalculateAccountSummary(
  newParams: {
    principalCredit?: number;
    interestSettings?: InterestSettings;
    repaymentWindow?: number;
    createdAt?: Date;
    penaltySettings?: PenaltySettings;
  } = {},
  acc: RevolvingLoanAccount
): RevolvingLoanAccount {
  const paramsFromOldAcc = {
    principalCredit: acc.principalCredit,
    interestSettings: acc.interestSettings,
    repaymentWindow: acc.repaymentWindow,
    createdAt: acc.accountCreatedAt,
    penaltySettings: acc.penaltySettings
  };

  const accCreationParams = {
    ...paramsFromOldAcc,
    ...newParams,
    loans: [],
    repayments: []
  };

  const newAccount = createRevolvingAccount(accCreationParams);
  const events: LoanEvent[] = aggregateLogs(acc);

  const finalAcc = applyActions(events, newAccount);

  return finalAcc;
}

let acc = createRevolvingAccount({});

console.info(acc);
//
acc = takeLoan(12000, acc);
console.info('=========== acc ==============', acc);
console.info('\n\n\n==============\n\n\n');
acc = repayLoan(6000, acc);
console.info('=========== acc ==============', acc);
console.info('\n\n\n==============\n\n\n');
acc = repayLoan(3000, acc);
console.info('=========== acc ==============', acc);
console.info('\n\n\n==============\n\n\n');
acc = takeLoan(4000, acc);

console.info('=========== acc ==============', acc);

acc = repayLoan(3100, acc);
console.info('=========== acc ==============', acc);

acc = recalculateAccountSummary(
  {
    principalCredit: 20000,
    interestSettings: {
      interestCalculationMethod: 'FLAT',
      interestRateSettings: {
        daysInYear: 365,
        interestChargeFrequency: 'DAILY',
        interestRate: 0.14
      },
      interestType: 'SIMPLE',
      interestTimePeriodCountCriteria: 'REPAYMENT_PERIODICITY'
    }
  },
  acc
);

console.info('========loans========\n', acc.loans);
console.info('========repayments========\n', acc.repayments);
