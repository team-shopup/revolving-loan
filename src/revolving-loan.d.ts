export type LoanEventType = 'REQUEST_LOAN' | 'REPAYMENT';
export type LoanStatus = 'repayment-pending' | 'repaid' | 'partially-repaid';

export type LoanEvent = (Loan | Repayment) & {
  eventType: LoanEventType;
};
export type INTEREST_CALCULATION_PERIOD =
  | 'DAILY'
  | 'WEEKLY'
  | 'MONTHLY'
  | 'YEARLY';

type PenaltyCalculationMethod = 'NONE' | 'FLAT_FEE' | 'FLAT_FEE_WITH_INTEREST';
type InterestCalculationMethod = 'FLAT' | 'DECLINING_BALANCE';
type InterestType = 'SIMPLE' | 'COMPOUND';
type InterestChargeFrequency = 'DAILY' | 'WEEKLY' | 'MONTHLY' | 'YEARLY';
type InterestTimePeriodCountCriteria =
  | 'ACTUAL_DAYS_COUNT'
  | 'REPAYMENT_PERIODICITY';

interface PenaltySettings {
  penaltyInterestRate: number;
  penaltyFlatFee: number;
  penaltyCalculationMethod: PenaltyCalculationMethod;
}

interface InterestSettings {
  interestCalculationMethod: InterestCalculationMethod;
  interestTimePeriodCountCriteria: InterestTimePeriodCountCriteria;
  interestType: InterestType;
  interestRateSettings: {
    interestRate: number;
    daysInYear: 365 | 360;
    interestChargeFrequency: InterestChargeFrequency;
  };
}

export interface Loan {
  amount: number; // debt amount
  interestSettings: InterestSettings;
  repaymentWindow: number; // number of days before the debt has to be repaid
  repaymentDeadline: Date;
  penaltySettings: PenaltySettings;
  totalRepayable: number;
  remainingRepayable: number;
  repayable: {
    total: {
      principal: number;
      interest: number;
      total: number;
    };
    outstanding: {
      principal: number;
      interest: number;
      total: number;
    };
  };
  amountRepaid: number;
  repaymentBreakdown: {
    principalRepaid: number;
    interestRepaid: number;
  };
  status: LoanStatus;
  createdAt: Date;
}

export interface RevolvingLoanAccount {
  principalCredit: number;
  remainingCredit: number;
  outstanding: {
    principal: number;
    interest: number;
  };
  interestSettings: InterestSettings;
  repaymentWindow: number; // number of days before the debt has to be repaid,
  accountCreatedAt: Date;
  penaltySettings: PenaltySettings;
  loans: Loan[];
  repayments: Repayment[];
}

export interface Repayment {
  amount: number; // amount repaid
  createdAt: Date; // repayment date
}
